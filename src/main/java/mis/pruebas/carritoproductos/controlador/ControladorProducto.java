package mis.pruebas.carritoproductos.controlador;

import mis.pruebas.carritoproductos.modelo.Producto;
import mis.pruebas.carritoproductos.servicio.ServicioGenerico;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.EntityLinks;
import org.springframework.hateoas.server.ExposesResourceFor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin
@RequestMapping("/almacen/v1/productos")
@ExposesResourceFor(Producto.class)
public class ControladorProducto {

    @Autowired
    ServicioGenerico<Producto> servicioProducto; // Inyección de Dependencias

    @Autowired
    EntityLinks entityLinks;

    @GetMapping
    public CollectionModel<EntityModel<Producto>> obtenerProductos() {

        // EntityModel<T> {..}
        // CollectionModel<T> [{},...]
        // Link { url, rel }

        final Map<Long, Producto> mp = this.servicioProducto.obtenerTodos();
        final List<Producto> pp = new ArrayList<>();
        for(Long id: mp.keySet()) {
            final Producto p = mp.get(id);
            p.setId(id);
            pp.add(p);
        }

        return CollectionModel.of(
                pp.stream().map(p -> obtenerRespuestaProducto(p)).collect(Collectors.toUnmodifiableList())
        ).add(linkTo(methodOn(this.getClass()).obtenerProductos()).withSelfRel());
    }

    @GetMapping("/{idProducto}")
    public EntityModel<Producto> obtenerProductoPorId(@PathVariable(name = "idProducto") long id) {
        try {
            final Producto p = this.servicioProducto.obtenerPorId(id);
            return obtenerRespuestaProducto(p);
        } catch(Exception x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public ResponseEntity<EntityModel<Producto>> crearProducto(@RequestBody Producto p) {
        // Location: http://localhost:9999/almacen/v1/productos/1
        final long id = this.servicioProducto.agregar(p);
        p.setId(id);
        return ResponseEntity
                .ok()
                .location(crearEnlaceProducto(p).toUri())
                .body(obtenerRespuestaProducto(p))
                ;
    }

    @PutMapping("/{idProducto}")
    public void reemplazarProductoPorId(@PathVariable(name = "idProducto") long id,
                                        @RequestBody Producto p) {
        try {
            p.setId(id);
            this.servicioProducto.reemplazarPorId(id, p);
        } catch(Exception x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    static class ParcheProducto {
        public Double precio;
        public Double cantidad;
    }
    @PatchMapping("/{idProducto}")
    public void emparcharProductoPorId(@PathVariable(name = "idProducto") long id,
                                        @RequestBody ParcheProducto p) {
        try {

            final Producto o = this.servicioProducto.obtenerPorId(id);
            if(p.cantidad != null)
                o.setCantidad(p.cantidad);
            if(p.precio != null)
                o.setPrecio(p.precio);
        } catch(Exception x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{idProducto}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void borrarProductoPorId(@PathVariable(name = "idProducto") long id) {
        this.servicioProducto.borrarPorId(id);
    }

    private Link crearEnlaceProducto(Producto p) {
        return this.entityLinks
                .linkToItemResource(p.getClass(), p.getId())
                .withSelfRel()
                .withTitle("Detalles de este producto");
    }

    private List<Link> crearEnlacesAdicionalesProducto(Producto p) {
        return Arrays.asList(
                crearEnlaceProducto(p),

                linkTo(methodOn(this.getClass()).obtenerProductos())
                        .withRel("productos").withTitle("Todos los productos"),

                linkTo(methodOn(ControladorProveedoresProducto.class).obtenerProveedoresProducto(p.getId()))
                        .withRel("proveedores").withTitle("Lista de proveedores")
        );
    }

    private EntityModel<Producto> obtenerRespuestaProducto(Producto p) {
        return EntityModel.of(p).add(crearEnlacesAdicionalesProducto(p));
    }

}
